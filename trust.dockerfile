FROM rust:1.46-slim-buster

RUN mkdir /trust/

WORKDIR /trust/

RUN apt-get -qq update && apt-get -qq install libpq5 -y

COPY ./trust ./trust

COPY ./.conf ./.conf

COPY ./private.pem ./private.pem

COPY ./public.pem ./public.pem

RUN chmod +x /trust/trust

ENTRYPOINT ["/trust/trust", "run"]