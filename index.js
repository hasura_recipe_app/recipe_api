const express = require("express");
const cors = require("cors");
require("dotenv").config({ path: "./.env" });

const app = express();
const port = process.env.EXPRESS_PORT || "8000";

app.use(cors());
app.use(express.static("./public"));
app.use(express.json({ limit: "50MB" }));

// ROUTES TO (CONNECTS TO HASURA ACTIONS)
app.post("/uploadImages", require("./handlers/upload"));
app.post("/subscribe_recipe_event", require("./routes/subscribeRecipe"));
app.post("/login", require("./routes/login.hook"));

app.listen(port, () => {
  console.log(`Listening to requests on http://localhost:${port} And Success`);
});
