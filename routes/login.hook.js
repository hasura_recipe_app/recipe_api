"use strict";
const gql = require("graphql-tag");
const apollo_client = require("../handlers/apollo");

module.exports = async (req, res) => {
  try {
    const { user } = req.body;

    let result = await apollo_client.mutate({
      mutation: gql`
        mutation ($user: recipeUser_insert_input!) {
          insert_recipeUser_one(
            object: $user
            on_conflict: {
              constraint: recipeUser_pkey
              update_columns: [id, email]
            }
          ) {
            id
            email
            username
            avatar
          }
        }
      `,

      variables: {
        user: {
          id: user.id,
          email: user.email,
          username: user.name,
        },
      },
    });

    let { data } = result;

    console.log("Data - ", data);

    res.send({
      "x-hasura-user-id": data.insert_recipeUser_one.id,
      "x-hasura-allowed-roles": ["admin", "user"],
      "x-hasura-default-role": "user",
      username: data.insert_recipeUser_one.username,
      email: data.insert_recipeUser_one.email,
      avatar: data.insert_recipeUser_one.avatar,
    });
  } catch (error) {
    console.error(error);
    throw error;
  }
};
