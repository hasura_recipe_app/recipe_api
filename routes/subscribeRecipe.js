const gql = require("graphql-tag");
const apollo_client = require("../handlers/apollo");

// FUNCTION TO STORE NEW CREATED RECIPE TO NOTIFICATION TABLE FROM EVENT PAYLOAD
const subscribeRecipe = async (req, res) => {
  let iEvent = req.body.event;

  let responseBody = "";

  try {
    if (iEvent.op === "INSERT") {
      responseBody = `New Recipe ${iEvent.data.new.id} inserted, with title: ${iEvent.data.new.title}`;

      let { data } = await apollo_client.mutate({
        mutation: gql`
          mutation ($message: String!, $recipeId: Int!, $userId: String!) {
            insert_notification_one(
              object: {
                message: $message
                recipeId: $recipeId
                userId: $userId
              }
            ) {
              id
            }
          }
        `,
        variables: {
          message: responseBody,
          recipeId: iEvent.data.new.id,
          userId: iEvent.data.new.userId,
        },
      });

      console.log("Data from Subscribe - ", data);
    }

    res.status(200).json(responseBody);
  } catch (e) {
    return res.status(400).json({
      message: e.message,
    });
  }
};

module.exports = subscribeRecipe;
