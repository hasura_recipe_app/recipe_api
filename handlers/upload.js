const cloudinary = require("cloudinary").v2;

module.exports = async (req, res) => {
  try {
    const { image, folder } = req.body.input;

    let secure_urls = [];

    let urls = [];

    for (let im in image) {
      im = image[im];

      let data = await cloudinary.uploader.upload(im, {
        unique_filename: true,
        discard_original_filename: true,
        folder: folder,
        timeout: 120000,
      });

      secure_urls.push(data.secure_url);

      urls.push(data.url);
      console.log("Secure Url", secure_urls);
      console.log("Url - ", urls);
    }

    res.send({
      secure_urls,
      urls,
    });
  } catch (error) {
    console.error(error);

    res.status(500).send({
      message: "Error Uploading Files",
      code: "error_uploading_file",
    });
  }
};
